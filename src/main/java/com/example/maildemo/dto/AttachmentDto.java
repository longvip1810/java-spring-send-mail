package com.example.maildemo.dto;

import com.example.maildemo.enums.ContentType;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AttachmentDto {

    private String fileName;

    private ContentType contentType;

    private String base64Content;

}
