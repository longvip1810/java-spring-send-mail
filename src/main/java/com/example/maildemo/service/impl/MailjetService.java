package com.example.maildemo.service.impl;

import com.example.maildemo.dto.EmailDto;
import com.example.maildemo.service.MailService;
import com.google.gson.Gson;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.resource.Emailv31;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class MailjetService implements MailService {

    @Value("${mail.impl.mailjet.api-key}")
    private String apiKey;

    @Value("${mail.impl.mailjet.secret-key}")
    private String secretKey;

    @Value("${mail.impl.mailjet.version}")
    private String mailJetVersion;

    @Override
    public boolean sendEmail(EmailDto emailDto) {
        MailjetClient client = new MailjetClient(apiKey, secretKey, new ClientOptions(mailJetVersion));
        MailjetRequest request = new MailjetRequest(Emailv31.resource)
                .property(Emailv31.MESSAGES, new JSONArray()
                        .put(new JSONObject(new Gson().toJson(emailDto))));
        try{
            MailjetResponse response = client.post(request);
            System.out.println(response.getStatus());
            System.out.println(response.getData());
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
