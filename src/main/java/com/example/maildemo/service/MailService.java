package com.example.maildemo.service;

import com.example.maildemo.dto.EmailDto;

public interface MailService {
    boolean sendEmail(EmailDto emailDto);
}
