package com.example.maildemo;

import com.example.maildemo.dto.AttachmentDto;
import com.example.maildemo.dto.EmailDto;
import com.example.maildemo.dto.MailerDto;
import com.example.maildemo.enums.ContentType;
import com.example.maildemo.service.MailService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

@SpringBootApplication
public class MailDemoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(MailDemoApplication.class, args);
    }

    @Value("${mail.impl.mailjet.host-mail}")
    private String hostEmail;

    @Value("${mail.impl.mailjet.host-mail}")
    private String hostName;

    @Autowired
    private MailService mailService;

    @Override
    public void run(String... args) throws Exception {
        MailerDto toMailer = new MailerDto("longvip1810@gmail.com", ""); //mail to
        List<MailerDto> toMailerList = Arrays.asList(toMailer);

        //Create mail body
        String mailBody = FileUtils.readFileToString(new ClassPathResource("welcome-mail.html").getFile(), "UTF-8");
        //Pasting dynamic content to mail template
        Map<String, String> mailAttributes = new HashMap<>();
        mailAttributes.put("name", toMailer.getName());
        StringSubstitutor stringSubstitutor = new StringSubstitutor(mailAttributes);
        mailBody = stringSubstitutor.replace(mailBody);

        //Create attachments
        File attachedFile = new ClassPathResource("note.txt").getFile();
        byte[] fileBytes = IOUtils.toByteArray(new FileInputStream(attachedFile));
        String base64content = Base64.getEncoder().encodeToString(fileBytes);
        AttachmentDto attachment = new AttachmentDto(attachedFile.getName(), ContentType.TEXT_PLAIN, base64content);
        List<AttachmentDto> attachments = Arrays.asList(attachment);

        //Create email dto
        EmailDto email = EmailDto.builder()
                .from(new MailerDto(hostEmail, hostName))
                .to(toMailerList)
                .subject("Mail demo greetings")
                .htmlPart(mailBody)
                .attachments(attachments)
                .build();

        //Send email
        System.out.println(mailService.sendEmail(email) ? "Send success" : "Send failed");

    }
}
